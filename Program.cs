using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;


namespace SMSSender
{
    class Program
    {
        private const string endpoint = "http://api.linkmobility.dk/v2/message.json?apikey=" + apikey;
        private const string apikey = "<apikey>";

        static void Main(string[] args)
        {
            // Create the SMS
            SMS sms = new SMS();
            sms.recipients = "<phone number>"; 
            sms.sender = "DEMO";
            sms.message = "Hello world";

            // Creating http client
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(endpoint);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Getting message object 
            SMS.Message message = sms.getMessage();

            // Serialize message object to JSON
            string json = new JavaScriptSerializer().Serialize(message);

            // Generating body content and set header Content-Type
            StringContent sc = new StringContent(json, Encoding.UTF8, "application/json");

            // Making request
            HttpResponseMessage response = client.PostAsync("", sc).Result;

            // If status code is 2xx
            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine("SMS successfull sendt");
                var content = response.Content.ReadAsStringAsync();
                Console.WriteLine(content.Result);
            }
            else
            {
                Console.WriteLine("An error had happend");
                var contents = response.Content.ReadAsStringAsync();
                Console.WriteLine(contents.Result);
                
            }

            Console.ReadLine();

        }
    }
}
