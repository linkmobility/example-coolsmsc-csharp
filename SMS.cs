namespace SMSSender
{
    class SMS
    {

        public struct Message
        {
            public SMS message;
        }

        public string recipients;
        public string sender;
        public string message;

        public Message getMessage()
        {
            Message m = new Message();
            m.message = this;

            return m;
            
        }
    }
}
